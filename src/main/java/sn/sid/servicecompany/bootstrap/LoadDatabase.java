package sn.sid.servicecompany.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sn.sid.servicecompany.domain.Company;
import sn.sid.servicecompany.repository.CompanyRepository;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    public CommandLineRunner initDatabase(CompanyRepository companyRepository) {
        return args -> {
            log.info("Create new " + companyRepository.save((new Company(null, "ATPS", 100 + Math.random() * 90))));
            log.info("Create new " + companyRepository.save((new Company(null, "SCL", 100 + Math.random() * 90))));
            log.info("Create new " + companyRepository.save((new Company(null, "ATOS", 100 + Math.random() * 90))));

            companyRepository.findAll().forEach(System.out::println);
            log.info("Company Count: " + companyRepository.count());
        };
    }
}
